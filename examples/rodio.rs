extern crate gme_rodio;
extern crate rodio;

use rodio::Source;

fn main() {
    let file = "test.nsf"; /* Default file to open */

    // index of track to play (0 = first)
    //int track = argc >= 3 ? atoi(argv[2]) : 0;
    let track = 0;

    let endpoint = rodio::get_default_endpoint().unwrap();
    let mut source = gme_rodio::GmeSource::new(file, 44100);
    source.set_track(track);
    rodio::play_raw(&endpoint, source.convert_samples());

    loop {}
}
