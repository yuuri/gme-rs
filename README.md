gme-rs
=========

A __Rust__ binding for the [GME library] (the Game Music Emu library).

## Installation

This binding requires the [GME library] library to be installed:

```Shell
# on debian based systems:
sudo apt-get install libgme-dev
```

This crate works with Cargo and will be on [crates.io]. When it will, you'll be
able to just add the following to your `Cargo.toml` file:

```toml
[dependencies]
gme = "*"
```

And add the following line to your source code:

```rust
extern crate gme;
```

## Building

Just run `cargo build`.

## Examples

Run `cargo run --example demo`.

## License
__gme-rs__ is a wrapper for __GME__, therefore inherits the [LGPL 2.1 license](https://opensource.org/licenses/LGPL-2.1).

[GME library]: https://bitbucket.org/mpyne/game-music-emu/
