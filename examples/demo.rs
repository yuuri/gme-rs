extern crate hound;
extern crate gme;

fn main() {
    let file = "test.nsf"; /* Default file to open */

    // index of track to play (0 = first)
    //int track = argc >= 3 ? atoi(argv[2]) : 0;
    let track = 0;

    // Create emulator and set sample rate
    //Music_Emu* emu = file_type->new_emu();
    //if ( !emu )
    //    handle_error( "Out of memory" );
    //handle_error( emu->set_sample_rate( sample_rate ) );
    let spec = hound::WavSpec {
        channels: 1,
        sample_rate: 44100,
        bits_per_sample: 16,
        sample_format: hound::SampleFormat::Int,
    };

    let mut emu = gme::Gme::new(file, spec.sample_rate);

    // Start track
    //handle_error( emu->start_track( track ) );
    emu.start_track(track);

    // Begin writing to wave file
    //Wave_Writer wave( sample_rate, "out.wav" );
    //let wave = WaveWriter::new(sample_rate, "out.wav");
    //wave.enable_stereo()


    let mut wave = hound::WavWriter::create("out.wav", spec).unwrap();

    // Record 10 seconds of track
    //while ( emu->tell() < 10 * 1000L )
    //{
    while emu.tell() < 10 * 1000 {
        // Sample buffer
        //const long size = 1024; // can be any multiple of 2
        //short buf [size];
        const SIZE: usize = 1024;
        let mut writer = wave.get_i16_writer(SIZE as u32);
        let mut buf: [gme::Sample; SIZE] = [0; SIZE];

        // Fill buffer
        //handle_error( emu->play( SIZE, buf ) );
        emu.play(&mut buf);

        // Write samples to wave file
        //wave.write( buf, SIZE );
        //wave.write(buf);
        for s in buf.iter() {
            writer.write_sample(*s);
        }
        writer.flush();
    }
}
