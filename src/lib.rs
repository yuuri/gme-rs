extern crate libc;
use libc::{c_void, c_char, c_short, c_int};
use std::ffi::CString;
use std::ptr;

#[repr(C)]
pub enum GmeType {
    AY,   //gme_ay_type,
    GBS,  //gme_gbs_type,
    GYM,  //gme_gym_type,
    HES,  //gme_hes_type,
    KSS,  //gme_kss_type,
    NSF,  //gme_nsf_type,
    NSFE, //gme_nsfe_type,
    SAP,  //gme_sap_type,
    SPC,  //gme_spc_type,
    VGM,  //gme_vgm_type,
    VGZ   //gme_vgz_type;
}

/*pub enum GmeError {
    UnknownType
}

pub type GmeResult<T> = Result<T, GmeError>*/

type gme_err_t = *const c_char;

struct MusicEmu;

#[link(name = "gme")]
extern {
    fn gme_open_file(path: *const c_char, out: *mut *mut MusicEmu, sample_rate: c_int) -> gme_err_t;
    fn gme_start_track(emu: *mut MusicEmu, index: c_int) -> gme_err_t;
    fn gme_play(emu: *mut MusicEmu, count: c_int, buf: *mut c_short) -> gme_err_t;
    fn gme_delete(emu: *mut MusicEmu) -> c_void;
    fn gme_tell(emu: *const MusicEmu) -> c_int;
}

/*
pub fn identify_file(filename: &str) -> Option<GmeType> {
    Some(NSF)
}
*/

pub type Sample = i16;

pub struct Gme {
    emu: *mut MusicEmu
}

unsafe impl Send for Gme {}

impl Gme {
    pub fn new(filename: &str, sample_rate: u32) -> Gme {
        let mut emu: *mut MusicEmu = ptr::null_mut();
        let filename = CString::new(filename).unwrap();
        unsafe {
            gme_open_file(filename.as_ptr(), &mut emu, sample_rate as c_int);
        }
        Gme {emu: emu}
    }

    pub fn start_track(&mut self, track: u8) {
        unsafe {
            gme_start_track(self.emu, track as c_int);
        }
    }

    pub fn play(&mut self, buf: &mut [Sample]) {
        unsafe {
            gme_play(self.emu, buf.len() as c_int, buf.as_mut_ptr());
        }
    }

    pub fn tell(&self) -> usize {
        unsafe {
            gme_tell(self.emu) as usize
        }
    }
}

impl Drop for Gme {
    fn drop(&mut self) {
        unsafe {
            gme_delete(self.emu);
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
    }
}
