extern crate gme;
extern crate rodio;

use gme::*;

const BUF_SIZE: usize = 1024;

pub struct GmeSource {
    buf: [Sample; BUF_SIZE],
    cursor: usize,
    gme: Gme
}

impl GmeSource {
    pub fn new(file: &str, sample_rate: u32) -> GmeSource {
        GmeSource {
            buf: [0; BUF_SIZE],
            cursor: 0,
            gme: Gme::new(file, sample_rate)
        }
    }

    pub fn set_track(&mut self, track: u8) {
        self.gme.start_track(track)
    }
}

impl Iterator for GmeSource {
    type Item = gme::Sample;

    fn next(&mut self) -> Option<Self::Item> {
        self.cursor += 1;
        if self.cursor >= BUF_SIZE {
            self.gme.play(&mut self.buf);
            self.cursor = 0
        }
        Some(self.buf[self.cursor])
    }
}

impl rodio::Source for GmeSource {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        1
    }

    fn samples_rate(&self) -> u32 {
        44100
    }

    fn total_duration(&self) -> Option<std::time::Duration> {
        None
    }
}
